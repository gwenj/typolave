# Typolave --- Caractères de signalisation
Version 0.86

Mai 2022

Gwenaël Jouvin - gwenael.jouvin@laposte.net


## 1 --- Préambule
Cette version 0.85
n’est diffusée que pour essais préalables
et n’est pas une version aboutie.
Elle reste à compléter
et peut comporter
des défauts de dessin ou de réglage.

L’utilisateur ou l’utilisatrice est encouragée à signaler tout défaut à l’auteur.


## 2 --- Généralités

### 2.1 --- Description

La collection de polices « Typolave »
permet de reproduire les caractères utilisés sur les signaux routiers fabriqués par la société Michelin
entre 1948 et 1971.

Deux groupes de polices se distinguent :

* les linéales utilisées pour les mentions courantes de direction,
classées en 3 polices de la plus large (L1) à la plus étroite (L3) ;

* les cursives, utilisées pour les mentions d’intérêt local,
composées avec la police L4.

La police « Lh » est principalement utilisée pour la partie hectométrique des indications de distance,
plus rarement pour les cartouches.
Elle ne comporte que des chiffres et la virgule.

### 2.2 --- Nom
Le nom « Typolave » donné à cette collection de fontes
fait directement référence à la fois à la typographie
et au matériau utilisé pour la fabrication des panneaux
où ces fontes étaient utilisées.

### 2.3 --- Correspondance avec la réglementation
Les caractères « haut-de-casse » des polices L1, L2 et L3
correspondent à ceux définis dans l’instruction générale sur la signalisation de 1946,
à quelques détails près puisque cette instruction ne précisait pas
les caractéristiques des accents ou de la cédille.

La fonte L4 est propre à Michelin,
même si elle présente une forte similitude
avec la planche L4
de l’instruction interministérielle de 1955.


## 3 --- Installation

### 3.1 --- Systèmes de type Unix

Copier les fichiers de fonte d’extension `ttf`
vers un des répertoires
`~/.fonts`, `~/.local/share/fonts` ou `/usr/share/fonts`
puis exécuter la commande `$ fc-cache -fv` pour les référencer.

### 3.2 --- Apple

Voir la documentation du constructeur :
http://support.apple.com/fr-fr/HT201749

### 3.3 --- Windows

Copier les fichiers de fonte d’extension `ttf`
vers le répertoire
`C:\Windows\fonts`.


## 4 --- Structure des polices

### 4.1 --- Haut de casse
Les lettres situées dans les emplacements réservés aux « capitales »
ont été dessinées d’après les planches cotées
annexées aux instructions sur la signalisation publiées en 1946 et en 1955.
En particulier, le P et le R correspondent à l’instruction de 1946,
les accents à celle de 1955.

Le « Ç » utilisé par Michelin est propre à ce fabriquant et a été tracé d’après des photos de panneaux.

Les caractères « Œ » des polices L1 à L3 ont été créés arbitrairement en s’inspirant de photographies
et de l’instruction sur la signalisation de 2002 qui les ont fait apparaître.

Les caractères « Œ », « œ », « Æ » et « æ » de la police L4 ont été dessinés arbitrairement sans modèle.
Notons que la lettre « æ » est peu susceptible d’être utilisée en langue française.

Le caractère « ĳ » a également été mis à disposition tant il était simple à réaliser,
mais est peu susceptible d’être utilisé dans un contexte francophone.

Les caractères de la police Lh sont propres à Michelin
et ont été dessinés d’après des panneaux en place.


### 4.2 --- Bas de casse
Les caractères bas de casse des polices L1 et L2
correspondent aux lettres de hauteur 60 mm
utilisées pour composer les toponymes très longs.
Ces lettres ont été pour la plupart
dessinées d’après des panneaux en place.

Les caractères de 40 mm de hauteur parfois rencontrés
feront l’objet d’une police spéciale.


### 4.3 --- Lettres supérieures
En zone privée (unicode u+f03a et suivants),
une gamme complète de lettre supérieures
et de ligatures supérieures sont mises à disposition
afin de composer les suffixes rencontrés dans certaines mentions abrégées.

Ces lettres et leurs ligatures sont accessibles
en activant les options Opentype
`sups` et `dlig`.


## 5 --- Fonctions

### 5.1 --- Crénage

Le crénage est ajusté pour que les différents caractères s’affichent de manière harmonieuse.

Les approches des fontes L1 à L3 sont réglées de manière à avoir un peu plus d’une épaisseur de trait entre chaque lettre,
comme préconisé dans l’article 81 de l’instruction interministérielle sur la signalisation routière de 1955 :

« Les espacements entre les caractères ont une très grande importance pour la lisibilité […]
Il faut alors que les espacements soient au moins égaux à l'épaisseur du trait.
En les majorant de 10 % à 20%, on accroît encore la lisibilité, surtout pour la lettre I. »

Des approches droite et gauche sont déterminées arbitrairement pour chaque lettre
et des paires de crénage sont définies pour différents groupes de lettres.

Les approches de la fonte L4 sont déterminées de manière subjective,
d’après les règles générales utilisées en typographie
(voir K. Cheng, *Designing Type*, 2020).


Le crénage, essentiel à une bonne composition typographique,
est activé par la fonction Opentype `kern`.


### 5.2 --- Lettres supérieures

La composition de certaines mentions longues nécessite l’utilisation d’abréviations,
comme « S<sup>T</sup> » pour « SAINT », « CH<sup>AU</sup> » pour « CHÂTEAU » ou « CH<sup>PS</sup> » pour « CHAMPS ».
Pour faciliter leur lecture,
ces abréviations sont composées à l’aide de lettres d’un plus petit format,
alignées sur la ligne supérieure du texte.

Ces caractères sont accessibles à l’aide de la fonction Opentype `sups`.


### 5.3 --- Ligatures

En raison de la nécessité d’une excellente lisibilité
obtenue entre autre grâce à des espacements importants,
les caractères de signalisation ne comportent pas de ligatures
comme on le trouve
dans les caractères usuels.
Cependant, on a regroupé ici sous le terme « ligatures »
et la fonction Opentype `dlig`
des caractères spéciaux destinés à remplacer,
sous certaines conditions,
les caractères ordinaires.

Ainsi, lorsque la fonction `dlig` est activée,
la suite de lettres « SOUS »
est remplacée par le caractère spécial « <sup>S</sup>/<sub>S</sub> ».

Afin de faciliter la composition en lettres supérieures
fournies par la fonction `sups`,
plusieurs ensembles d’usage commun sont définis :
« <sup>AU</sup> » pour « CH<sup>AU</sup> », « ONS » pour « M<sup>ONS</sup>-ALFORT », « TE » pour « S<sup>TE</sup> », etc.


## 6 --- Hauteurs de composition

Les signaux peuvent être composés avec des caractères de hauteurs prédéterminées,
de 40 mm, 60 mm, 80 mm, 100 mm, 150 mm et 200 mm.

Les différentes combinaisons de hauteur possibles ont imposé des choix
dans le dessin des polices.
La hauteur la plus souvent rencontrée étant 150 mm,
elle sert de référence dans la plupart des polices Typolave.


### 6.1 --- Police L1
Les caractères haut-de-casse servent de modèle
et n’ont pas de « hauteur propre ».
Les bas-de-casse sont ajustés
pour qu’utilisés avec des haut-de-casse de **150 mm**,
ils correspondent à une hauteur de 60 mm
comme on le rencontre couramment sur les panneaux.
Le rapport de hauteur est de 2/5.

Il est fourni 26 lettres surélevées en exposant,
principalement destinées à être utilisées dans des abréviations,
comme « S<sup>T</sup> » pour « SAINT », « CH<sup>AU</sup> » pour « CHÂTEAU », etc.
Il a été constaté
que la plupart des toponymes abrégés composés avec la fonte L1
l’étaient dans une hauteur de base de **100 mm**,
avec des lettres abrégées de 60 mm de hauteur.
Le rapport entre les capitales et les supérieures est par conséquent fixé à 3/5.


### 6.2 --- Police L2
Comme pour la police L1,
les caractères haut-de-casse servent n’ont pas de hauteur propre.
Les bas-de-casse sont ajustés pour que,
pour qu’utilisés avec des haut-de-casse de **150 mm**,
ils correspondent à une hauteur de 60 mm
comme on le rencontre couramment.
Le rapport de hauteur est de 2/5.

La hauteur des supérieures est ajustée de façon à être
de la gamme directement inférieure à celle des capitales,
avec un rapport de hauteur de 2/3.
Elles sont donc utilisables
avec une mention de **150 mm** de hauteur
et *apparaîtront* avec une hauteur de 100 mm.

Le rapport des hauteurs varie en fonction de la hauteur de la mention :

200 - 150 : rapport 3/4

**150 - 100 : rapport 2/3**

100 - 80 : rapport 4/5

Pour les cas où le rapport entre les lettres
est différent de 2/3,
les supérieures devront être composées et ajustées séparément.


### 6.3 --- Police L3
La police L3 ne comporte pas de caractères bas-de-casse.

Les supérieures sont ajustées de manière à être utilisées
avec des mentions de **150 mm** de hauteur,
avec un rapport de 2/3,
pour apparaître avec une hauteur de 100 mm.


### 6.4 --- Police L4
Les caractères de la police L4 sont ajustés
pour être composés avec les polices L1 à L3,
avec comme hauteur de base 140 mm
comme couramment rencontré sur les panneaux Michelin.

Les supérieures sont composées
dans la gamme directement inférieure.
Pour une hauteur de base de 140 mm,
la hauteur des supérieures est de 106 mm.


### 6.5 --- Police Lh

La police Lh semble n’exister qu’avec une hauteur de 80 mm
et est principalement utilisée avec les mentions
des villes proches,
dont les distances autorisent l’emploi d’hectomètres.

Sa hauteur est ajustée de manière à être utilisée telle quelle
avec les autres polices,
en supposant que ces dernières font **100 mm** de hauteur,
dimension généralement utilisée
pour les mentions éloignées de moins de 5 km,
seules à disposer d’une précision hectométrique
(instruction générale de 1946, art. 48).


## 7 --- Licence
La licence appliquée aux fichiers de police mis à disposition est la SIL OFL
reproduite dans le fichier LICENSE.md.

---

*Copyright (c) 2022, Gwenaël Jouvin (gwenael.jouvin@laposte.net),
with Reserved Font Name “Typolave”.*



## 8 --- À venir

Une documentation illustrée et plus aboutie
sera fournie avec la version stable des polices.

D’autres fontes sont en cours de réalisation
pour couvrir la période 1927-1946
et seront publiées ultérieurement.


### 8.1 --- Erreurs connues

Lorsqu’une lettre supérieure obtenue par `sups`
est placée après une lettre,
le crénage `kern` n’est pas activé
et l’espacement dépend exclusivement des approches des caractères.
Ceci peut poser problème avec la fonte L4 dont les caractères sont inclinés.
Le problème doit être contourné en intercalant un espace ou en découpant le texte en blocs indépendants.


## 9 --- Journal des modifications

### Version 0.86 -- 2022-05-04

README : corrections diverses, ajout des instructions d’installation.

Toutes polices : ajout de la ligature « <sup>NNE</sup> » pour l’abréviation de « MAURIENNE ».

Polices L1 à L3 : correction des tables de crénage.

Police L4 : correction de ligatures erronnées pour les lettres supérieures.


### Version 0.85 -- 2022-05-03

Version initiale.


## Table des matières 

1 --- Préambule

2 --- Généralités

 * 2.1 --- Description

 * 2.2 --- Nom

 * 2.3 --- Correspondance avec la réglementation

3 --- Installation

 * 3.1 --- Systèmes de type Unix

 * 3.2 --- Apple

 * 3.3 --- Windows

4 --- Structure des polices

 * 4.1 --- Haut de casse

 * 4.2 --- Bas de casse

 * 4.3 --- Lettres supérieures

5 --- Fonctions

 * 5.1 --- Crénage

 * 5.2 --- Lettres supérieures

 * 5.3 --- Ligatures

6 --- Hauteurs de composition

 * 6.1 --- Police L1

 * 6.2 --- Police L2

 * 6.3 --- Police L3

 * 6.4 --- Police L4

 * 6.5 --- Police Lh

7 --- Licence

8 --- À venir

 * 8.1 --- Erreurs connues

9 --- Journal des modifications
